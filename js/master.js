window.onload = function () {
  const canvas = document.getElementById("tetris");
  const context = canvas.getContext("2d");
  context.scale(20, 20);

  const player = {
    pos: { x: 0, y: 0 },
    matrix: null,
    scorePlayer: 0,
  };

  const bg = createMatrix(12, 20);
  playerReset();

  const colors = [
    "transparent",
    "red",
    "blue",
    "green",
    "yellow",
    "orange",
    "purple",
    "violet",
  ];
  function createPieces(type) {
    switch (type) {
      case "T": {
        return [
          [0, 0, 0],
          [0, 1, 0],
          [1, 1, 1],
        ];
      }
      case "L": {
        return [
          [0, 2, 0],
          [0, 2, 0],
          [0, 2, 2],
        ];
      }
      case "J": {
        return [
          [0, 3, 0],
          [0, 3, 0],
          [3, 3, 0],
        ];
      }
      case "S": {
        return [
          [0, 4, 4],
          [4, 4, 0],
          [0, 0, 0],
        ];
      }
      case "Z": {
        return [
          [5, 5, 0],
          [0, 5, 5],
          [0, 0, 0],
        ];
      }
      case "O": {
        return [
          [6, 6],
          [6, 6],
        ];
      }
      case "I": {
        return [
          [0, 7, 0, 0],
          [0, 7, 0, 0],
          [0, 7, 0, 0],
          [0, 7, 0, 0],
        ];
      }
    }
  }

  function playerReset() {
    const pieces = ["T", "J", "L", "S", "Z", "I", "O"];
    player.matrix = createPieces(pieces[Math.floor(Math.random() * 7)]);
    player.pos.y = 0;
    console.log(player.matrix[0]);
    player.pos.x =
      Math.floor(bg[0].length / 2) - Math.floor(player.matrix[0].length / 2);

    // game over
    if (collide(bg, player)) {
      bg.forEach((row) => row.fill(0));
      player.score = 0;
      score();
    }
  }

  // background matrix
  function createMatrix(w, h) {
    const matrix = [];
    while (h--) {
      matrix.push(new Array(w).fill(0));
    }
    return matrix;
  }

  function merge(bg, player) {
    player.matrix.forEach((row, y) => {
      row.forEach((value, x) => {
        if (value !== 0) {
          bg[y + player.pos.y][x + player.pos.x] = value;
        }
      });
    });
  }

  function drawMatrix(matrix, offset) {
    matrix.forEach((row, y) => {
      row.forEach((value, x) => {
        if (value) {
          context.fillStyle = colors[value];
          context.fillRect(x + offset.x, y + offset.y, 1, 1);
        }
      });
    });
  }

  function draw() {
    //   background
    context.fillStyle = "#242424";
    context.fillRect(0, 0, canvas.width, canvas.height);

    // draw shape
    drawMatrix(bg, { x: 0, y: 0 });
    drawMatrix(player.matrix, player.pos);
  }

  //  start drop shapes

  function collide(bg, player) {
    for (y = 0; y < player.matrix.length; ++y) {
      for (x = 0; x < player.matrix[y].length; ++x) {
        if (
          player.matrix[y][x] !== 0 &&
          (bg[y + player.pos.y] && bg[y + player.pos.y][x + player.pos.x]) !== 0
        ) {
          return true;
        }
      }
    }
    return false;
  }

  function playerDrop() {
    player.pos.y++;
    if (collide(bg, player)) {
      player.pos.y--;
      merge(bg, player);
      playerReset();
      sweep();
    }
    dropCounter = 0;
  }

  let lastTime = 0;
  let dropCounter = 0;
  let dropInterval = 1000;

  function update(time = 0) {
    const deltaTime = time - lastTime;
    lastTime = time;
    dropCounter += deltaTime;
    if (dropCounter > dropInterval) {
      playerDrop();
    }

    // end drop shape
    draw();
    requestAnimationFrame(update);
  }
  // move shape
  function playerMove(dir) {
    player.pos.x += dir;
    if (collide(bg, player)) {
      player.pos.x -= dir;
    }
  }

  //  rotate shape
  function rotate(matrix) {
    for (y = 0; y < matrix.length; ++y) {
      for (x = 0; x < y; ++x) {
        [matrix[x][y], matrix[y][x]] = [matrix[y][x], matrix[x][y]];
      }
    }
    matrix.reverse();
    const temp = player.pos.x;
    let offset = 1;
    while (collide(bg, player)) {
      console.log(matrix);
      player.pos.x += offset;
      offset = -(offset ? 1 : -1);
      if (offset > player.matrix[0].length) {
        rotate(player.matrix);
        player.pos.x = temp;
        return;
      }
    }
  }
  // sweep
  function sweep() {
    let rowCount = 1;
    outer: for (y = bg.length - 1; y > 0; --y) {
      for (x = 0; x < bg[y].length; ++x) {
        if (bg[y][x] === 0) {
          continue outer;
        }
      }
      const row = bg.splice(y, 1)[0].fill(0);
      bg.unshift(row);
      ++y;

      player.scorePlayer += rowCount + 9;
      rowCount += 2;
      score();
    }
  }

  // score
  function score() {
    document.getElementById("score").innerHTML = player.scorePlayer;
  }

  // add controls
  document.addEventListener("keydown", (event) => {
    console.log(event)
    if (event.keyCode === 39) {
      playerMove(1);
    } else if (event.keyCode === 37) {
      playerMove(-1);
    } else if (event.keyCode === 40) {
      playerDrop();
    } else if (event.keyCode === 32) {
      rotate(player.matrix);
    }
  });

  update();
};
